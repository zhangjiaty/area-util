# area-util

#### 介绍
地址解析工具,从字符中提取出,省市区,姓名,电话,参照https://github.com/akebe/address-parse 解析工具,改成java

#### 软件架构
工具类,根据省市区名称匹配出对应的省市区,获取编码,使用正则方式提取出手机号,姓名


#### 安装教程

打包编译之后,其他项目引用即可

#### 使用说明

1.  ParseArea,ParseArea2为解析工具类入口,ParseArea是参照https://github.com/akebe/address-parse 改写成的java代码,代码结构和逻辑都没有改变.
ParseArea2是参照ParseArea进行的代码重构,从测试的结果来看和ParseArea一致.
2.  AreaUtilTest中有测试的例子
3.  对照https://github.com/akebe/address-parse 新加了一个功能,某些地区有更名的情况,一个地方可能出现曾用名和现有名同时使用,为解决这种情况,在AreaData里面对相同地区码做了处理,以中杠 - 进行区分,中杠以前字符相同的是同一个地方,只是名称不一样,例如put("371403", "陵城区"),put("371403-1", "陵县");地区里面陵城区和陵县都可以解析成371403地区码,我暂时知道的就2个: put("371324", "兰陵县");put("371324-1", "苍山县");    put("371403", "陵城区"),put("371403-1", "陵县");,有其他的可以自行添加

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
