package cus.area.util;

import cn.hutool.json.JSONUtil;
import cus.area.util.entity.Area;
import org.junit.Test;

/**
 * 地址测试类
 */
public class AreaUtilTest {

    @Test
    public void formatStandardArea(){
        Area area = new Area.AreaBuilder().province("重庆").county("长寿").build();
        System.out.println(AreaUtils.formatStandardArea(area));
    }

    @Test
    public void getAreaByName(){
        Area area = new Area.AreaBuilder().county("丰原").build();
        System.out.println(AreaUtils.getAreaByName(area));
    }

    @Test
    public void parse(){
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea.parse("四川省成都市锦江区",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea.parse("北京市市辖区东城区",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("福建省福州市福清市石竹街道义明综合楼3F",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("福建省福清市石竹街道义明综合楼3F",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("福州市福清市石竹街道义明综合楼3F",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("福清市石竹街道义明综合楼3F",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("浙江省温州市乐清柳市镇",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("广西壮族自治区 桂林市 恭城瑶族自治县 恭城镇拱辰东路xx-xx号",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("恭城 恭城镇拱辰东路08-88号",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("四川成都高新区天府大道中段530号东方希望天祥广场a座4302号北京万商天勤(成都)律师事务所",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("山东省 德州市 乐陵市 市中街道怡然居小区",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("黑龙江省 哈尔滨市 道里区 经纬街道经纬七道街",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("深圳市龙华新区民治街道办民乐新村",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("内蒙古自治区 呼和浩特市 和林格尔县 盛乐经济工业园区内蒙古师范大学盛乐校区十三号楼",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("和林格尔 盛乐经济工业园区内蒙古师范大学盛乐校区十三号楼",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("上海市徐汇区 复兴中路1237号",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("龙湖区黄山路潮华雅居10栋000房 肖小姐",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("西安市雁塔区丈八东路晶城秀府7号楼2单元",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("湖北省安陆市西亚小铺",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("福建宁德福鼎太姥山镇岭后路",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("南京市雨花区小行路58号名城世家花园",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("四川省阆中市",false)));
    }

    @Test
    public void parseAll(){
//['福建省福州市福清市石竹街道义明综合楼3F，15000000000，asseek', '350181'],
//['福建省福清市石竹街道义明综合楼3F，15000000000，asseek', '350181'],
//['福州市福清市石竹街道义明综合楼3F，15000000000，asseek', '350181'],
//['福清市石竹街道义明综合楼3F，15000000000，asseek', '350181'],
//['浙江省温州市乐清柳市镇', '330382'],
//['李xx 13512222322 广西壮族自治区 桂林市 恭城瑶族自治县 恭城镇拱辰东路xx-xx号', '450332'],
//['李xx 13512222222 恭城 恭城镇拱辰东路08-88号', '450332'],
//['四川成都高新区天府大道中段530号东方希望天祥广场a座4302号北京万商天勤(成都)律师事务所', '510191'],
//'房pp,18263333333,山东省 德州市 乐陵市 市中街道怡然居小区,253600',
//'张y,18802222222,黑龙江省 哈尔滨市 道里区 经纬街道经纬七道街,000000',
//'深圳市龙华新区民治街道办民乐新村。陆xg15822222222',
//'张l,15222222222,内蒙古自治区 呼和浩特市 和林格尔县 盛乐经济工业园区内蒙古师范大学盛乐校区十三号楼,011500',
//['张l,15222222222,和林格尔 盛乐经济工业园区内蒙古师范大学盛乐校区十三号楼,011500', '150123'],
//'上海市徐汇区 复兴中路1237号 5楼WeWork 200010 柚子',
//['龙湖区黄山路潮华雅居10栋000房 肖小姐', '440507'],
//['西安市雁塔区丈八东路晶城秀府7号楼2单元     李飞', '610113'],
//['湖北省安陆市西亚小铺，文元13377777788', '420982'],
//['福建宁德福鼎太姥山镇岭后路。 丹', '350982'],
//['南京市雨花区小行路58号名城世家花园', '320114'],
//['四川省阆中市', '511381'],
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("福建省福州市福清市石竹街道义明综合楼3F，15000000000，asseek",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("福建省福清市石竹街道义明综合楼3F，15000000000，asseek",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("福州市福清市石竹街道义明综合楼3F，15000000000，asseek",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("福清市石竹街道义明综合楼3F，15000000000，asseek",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("浙江省温州市乐清柳市镇",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("李xx 13512222322 广西壮族自治区 桂林市 恭城瑶族自治县 恭城镇拱辰东路xx-xx号",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("李xx 13512222222 恭城 恭城镇拱辰东路08-88号",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("四川成都高新区天府大道中段530号东方希望天祥广场a座4302号北京万商天勤(成都)律师事务所",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("房pp,18263333333,山东省 德州市 乐陵市 市中街道怡然居小区,253600",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("张y,18802222222,黑龙江省 哈尔滨市 道里区 经纬街道经纬七道街,000000",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("深圳市龙华新区民治街道办民乐新村。陆xg15822222222",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("张l,15222222222,内蒙古自治区 呼和浩特市 和林格尔县 盛乐经济工业园区内蒙古师范大学盛乐校区十三号楼,011500",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("张l,15222222222,和林格尔盛乐经济工业园区内蒙古师范大学盛乐校区十三号楼,011500",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("上海市徐汇区 复兴中路1237号 5楼WeWork 200010 柚子",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("龙湖区黄山路潮华雅居10栋000房 肖小姐",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("西安市雁塔区丈八东路晶城秀府7号楼2单元     李飞",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("湖北省安陆市西亚小铺，文元13377777788",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("福建宁德福鼎太姥山镇岭后路。 丹",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("南京市雨花区小行路58号名城世家花园",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("四川省阆中市",false)));
//        System.out.println(JSONUtil.toJsonPrettyStr(ParseAddress.parse("江西南昌市青山湖区广兰大道418号东华理工大学核工系南区9栋1112室 131 1111 1111 孙轶念",false)));
        System.out.println(JSONUtil.toJsonPrettyStr(ParseArea2.parse("山东省临沂市兰陵田园路三号", false)));
    }
}
