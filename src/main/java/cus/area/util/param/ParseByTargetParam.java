package cus.area.util.param;

import cus.area.util.entity.ParseAreaResult;

import java.util.List;
import java.util.Map;

/**
 * 通过上级地址解析下级地址传递参数类,将参数汇总,用于重构,在ParseArea2中使用
 */
public class ParseByTargetParam {

    /**
     * 循环的entry
     */
    private Map.Entry<String, String> entry;

    /**
     * 地址
     */
    private String address;

    /**
     * 当前解析结果
     */
    private ParseAreaResult result;

    /**
     * 解析结果集合
     */
    private List<ParseAreaResult> results;

    private FindIndexFromAddress findIndexFromAddress;

    public Map.Entry<String, String> getEntry() {
        return entry;
    }

    public void setEntry(Map.Entry<String, String> entry) {
        this.entry = entry;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ParseAreaResult getResult() {
        return result;
    }

    public void setResult(ParseAreaResult result) {
        this.result = result;
    }

    public List<ParseAreaResult> getResults() {
        return results;
    }

    public void setResults(List<ParseAreaResult> results) {
        this.results = results;
    }

    public FindIndexFromAddress getFindIndexFromAddress() {
        return findIndexFromAddress;
    }

    public void setFindIndexFromAddress(FindIndexFromAddress findIndexFromAddress) {
        this.findIndexFromAddress = findIndexFromAddress;
    }
}
