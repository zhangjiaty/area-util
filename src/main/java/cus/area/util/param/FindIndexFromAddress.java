package cus.area.util.param;

import cn.hutool.core.util.StrUtil;

import java.util.Map;

public class FindIndexFromAddress {
        public String address;
        public Map.Entry<String, String> entry;
        public String code;
        public String name;
        public int nameLength;
        public int index;
        public String shortName;
        public Map<String, String> shorts;

        public FindIndexFromAddress(String address, Map.Entry<String, String> entry, Map<String, String> shorts) {
            this.address = address;
            this.entry = entry;
            this.shorts = shorts;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public int getNameLength() {
            return nameLength;
        }

        public int getIndex() {
            return index;
        }

        public String getShortName() {
            return shortName;
        }

        public FindIndexFromAddress invoke() {
            code = entry.getKey();
            name = entry.getValue();
            nameLength = name.length();
            index = address.indexOf(name);

            if (index == -1) {
                shortName = shorts.get(code);
                if(StrUtil.isNotEmpty(shortName)){
                    nameLength = shortName.length();
                    index = address.indexOf(shortName);
                }
            }
            return this;
        }
    }