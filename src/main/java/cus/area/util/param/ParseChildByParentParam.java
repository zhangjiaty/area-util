package cus.area.util.param;

import cus.area.util.entity.AreaEnum;
import cus.area.util.entity.ParseAreaResult;

import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

/**
 * 通过上级地址解析下级地址传递参数类,将参数汇总,用于重构,在ParseArea2中使用
 */
public class ParseChildByParentParam {

    /**
     * 地址
     */
    private String address;
    /**
     * 解析结果对象
     */
    private ParseAreaResult result;
    /**
     * 地址枚举
     */
    private AreaEnum areaEnum;
    /**
     * 地址对应缩写集合
     */
    private Map<String,String> shortMap;
    /**
     * 地址对应关键字集合,例如 省份中含有的 省,自治区
     */
    private Set<String> keys;
    /**
     * 判断是否进行缩写匹配后的关键字截取的方法
     */
    private BiFunction<String,String,Boolean> biFunction;
    /**
     * 匹配到名称之后处理code和name方法,第一个参数为code,第二个参数为name
     */
    private BiConsumer<String,String> biConsumer;

    /**
     * 下一个参数,如果有则会根据改条件再次进行解析
     */
    private ParseChildByParentParam nextParam;

    /**
     * index最大数字
     */
    private Integer maxIndex = Integer.MAX_VALUE;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ParseAreaResult getResult() {
        return result;
    }

    public void setResult(ParseAreaResult result) {
        this.result = result;
    }

    public AreaEnum getAreaEnum() {
        return areaEnum;
    }

    public void setAreaEnum(AreaEnum areaEnum) {
        this.areaEnum = areaEnum;
    }

    public Map<String, String> getShortMap() {
        return shortMap;
    }

    public void setShortMap(Map<String, String> shortMap) {
        this.shortMap = shortMap;
    }

    public Set<String> getKeys() {
        return keys;
    }

    public void setKeys(Set<String> keys) {
        this.keys = keys;
    }

    public BiFunction<String, String, Boolean> getBiFunction() {
        return biFunction;
    }

    public void setBiFunction(BiFunction<String, String, Boolean> biFunction) {
        this.biFunction = biFunction;
    }

    public BiConsumer<String, String> getBiConsumer() {
        return biConsumer;
    }

    public void setBiConsumer(BiConsumer<String, String> biConsumer) {
        this.biConsumer = biConsumer;
    }

    public ParseChildByParentParam getNextParam() {
        return nextParam;
    }

    public void setNextParam(ParseChildByParentParam nextParam) {
        this.nextParam = nextParam;
    }

    public Integer getMaxIndex() {
        return maxIndex;
    }

    public void setMaxIndex(Integer maxIndex) {
        this.maxIndex = maxIndex;
    }

    public static class ParseChildByParentParamBuilder{

        /**
         * 地址
         */
        private String address;
        /**
         * 解析结果对象
         */
        private ParseAreaResult result;
        /**
         * 地址枚举
         */
        private AreaEnum areaEnum;
        /**
         * 地址对应缩写集合
         */
        private Map<String,String> shortMap;
        /**
         * 地址对应关键字集合,例如 省份中含有的 省,自治区
         */
        private Set<String> keys;
        /**
         * 判断是否进行缩写匹配后的关键字截取的方法
         */
        private BiFunction<String,String,Boolean> biFunction;
        /**
         * 匹配到名称之后处理code和name方法,第一个参数为code,第二个参数为name
         */
        private BiConsumer<String,String> biConsumer;

        /**
         * 下一个参数,如果有则会根据改条件再次进行解析
         */
        private ParseChildByParentParam nextParam;

        /**
         * index最大数字
         */
        private Integer maxIndex = Integer.MAX_VALUE;


        public ParseChildByParentParamBuilder address(String address) {
            this.address = address;
            return this;
        }

        public ParseChildByParentParamBuilder result(ParseAreaResult result) {
            this.result = result;
            return this;
        }

        public ParseChildByParentParamBuilder areaEnum(AreaEnum areaEnum) {
            this.areaEnum = areaEnum;
            return this;
        }

        public ParseChildByParentParamBuilder shortMap(Map<String,String> shortMap) {
            this.shortMap = shortMap;
            return this;
        }

        public ParseChildByParentParamBuilder keys(Set<String> keys) {
            this.keys = keys;
            return this;
        }

        public ParseChildByParentParamBuilder biFunction(BiFunction<String,String,Boolean> biFunction) {
            this.biFunction = biFunction;
            return this;
        }

        public ParseChildByParentParamBuilder biConsumer(BiConsumer<String,String> biConsumer) {
            this.biConsumer = biConsumer;
            return this;
        }

        public ParseChildByParentParamBuilder nextParam(ParseChildByParentParam nextParam) {
            this.nextParam = nextParam;
            return this;
        }

        public ParseChildByParentParamBuilder maxIndex(Integer maxIndex) {
            this.maxIndex = maxIndex;
            return this;
        }

        public ParseChildByParentParam build() {
            ParseChildByParentParam param = new ParseChildByParentParam();
            param.setAddress(this.address);
            param.setResult(this.result);
            param.setAreaEnum(this.areaEnum);
            param.setShortMap(this.shortMap);
            param.setKeys(this.keys);
            param.setBiFunction(this.biFunction);
            param.setBiConsumer(this.biConsumer);
            param.setNextParam(this.nextParam);
            param.setMaxIndex(this.maxIndex);
            return param;
        }
    }
}
