package cus.area.util.entity;

import java.util.Optional;

public class Area {

    /**
     * 省
     */
    private String province;

    /**
     * 省编码
     */
    private String provinceCode;

    /**
     * 市
     */
    private String city;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 区
     */
    private String county;

    /**
     * 区编码
     */
    private String countyCode;

    /**
     * 地址
     */
    private String address;

    /**
     * 地址全称
     */
    private String fullAddress;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮编
     */
    private String zipCode;

    /**
     * 电话
     */
    private String phone;

    /**
     * 姓名
     */
    private String name;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullAddress() {
        return new StringBuilder().append(Optional.ofNullable(province).orElse("")).append(Optional.ofNullable(city).orElse("")).append(Optional.ofNullable(county).orElse("")).append(Optional.ofNullable(address).orElse("")).toString();
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class AreaBuilder {

        /**
         * 省
         */
        private String province;

        /**
         * 省编码
         */
        private String provinceCode;

        /**
         * 市
         */
        private String city;

        /**
         * 市编码
         */
        private String cityCode;

        /**
         * 区
         */
        private String county;

        /**
         * 区编码
         */
        private String countyCode;

        /**
         * 地址
         */
        private String address;

        /**
         * 手机
         */
        private String mobile;

        /**
         * 邮编
         */
        private String zipCode;

        /**
         * 电话
         */
        private String phone;

        /**
         * 姓名
         */
        private String name;

        public AreaBuilder province(String province) {
            this.province = province;
            return this;
        }

        public AreaBuilder provinceCode(String provinceCode) {
            this.provinceCode = provinceCode;
            return this;
        }

        public AreaBuilder city(String city) {
            this.city = city;
            return this;
        }

        public AreaBuilder cityCode(String cityCode) {
            this.cityCode = cityCode;
            return this;
        }

        public AreaBuilder county(String county) {
            this.county = county;
            return this;
        }

        public AreaBuilder countyCode(String countyCode) {
            this.countyCode = countyCode;
            return this;
        }

        public AreaBuilder address(String address) {
            this.address = address;
            return this;
        }

        public AreaBuilder mobile(String mobile){
            this.address = address;
            return this;
        }

        public AreaBuilder zipCode(String zipCode){
            this.zipCode = zipCode;
            return this;
        }

        public AreaBuilder phone(String phone){
            this.phone = phone;
            return this;
        }

        public AreaBuilder name(String name){
            this.name = name;
            return this;
        }

        public Area build() {
            Area area = new Area();
            area.setProvince(this.province);
            area.setProvinceCode(this.provinceCode);
            area.setCity(this.city);
            area.setCityCode(this.cityCode);
            area.setCounty(this.county);
            area.setCountyCode(this.countyCode);
            area.setAddress(this.address);
            area.setName(this.name);
            return area;
        }
    }

    @Override
    public String toString() {
        return "Area{" +
                "province='" + province + '\'' +
                ", provinceCode='" + provinceCode + '\'' +
                ", city='" + city + '\'' +
                ", cityCode='" + cityCode + '\'' +
                ", county='" + county + '\'' +
                ", countyCode='" + countyCode + '\'' +
                ", address='" + address + '\'' +
                ", fullAddress='" + fullAddress + '\'' +
                '}';
    }
}
